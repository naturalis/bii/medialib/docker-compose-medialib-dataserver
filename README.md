docker-compose-medialib-dataserver
 
=====================
docker-compose definition for deployment of nfs dataserver for medialibrary


Docker-compose
--------------

contains : 
 - ericough/nfs container

It is started using Foreman or ansible which installs docker and docker-compose and  creates:
 - .env file


Apparmor instruction
--------------------

apparmor should be configured to allow permissions for the container. the ericough-nfs-apparmor can be parsed to setup the correct rules. 

instruction: 
 - install apparmor-utils `sudo apt-get install apparmor-utils'
 - parse config, use provided file: `sudo apparmor_parser -r -W erichough-nfs-apparmor`
 - to make apparmor rule persistent copy file to /etc/apparmor.d : `sudo cp erichough-nfs-apparmor /etc/apparmor.d`

Result
------

Working NFS (v4 only) server with possibility for multiple exports. 

Limitations
-----------
This has been tested.
